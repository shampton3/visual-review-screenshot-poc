// function convertAllImagesToBase64(cloned) {
//   var pendingImagesPromises = [];
//   var pendingPromisesData = [];

//   var images = cloned.getElementsByTagName("img");
//   console.log(images);

//   for (var i = 0; i < images.length; i += 1) {
//     var promise = new Promise((resolve, reject) => {
//       pendingPromisesData.push({
//         index: i,
//         resolve,
//         reject
//       });
//     });
//     pendingImagesPromises.push(promise);
//   }

//   for (var i = 0; i < images.length; i += 1) {
//     var imageSrc = images[i].src;
//     fetch(imageSrc)
//       .then(response => {
//         console.log(response);
//         return response;
//       })
//       .then(data => {
//         console.log(imageSrc, data);
//         var pending = pendingPromisesData.find(p => p.index === i);
//         images[i].src = data;
//         pending.resolve(data);
//       })
//       .catch(e => {
//         console.log("HERE", images);
//         console.log("HERE", i);
//         console.log("HERE", images[i]);
//         console.log(imageSrc, e);
//         var pending = pendingPromisesData.find(p => p.index === i);
//         pending.reject(e);
//       });
//   }

//   return Promise.all(pendingImagesPromises);
// }

function objectToStyle(obj) {
  var result = "";
  for (var property in obj) {
    if (obj.hasOwnProperty(property)) {
      result += property + ": " + obj[property] + ";";
    }
  }
  return result;
}

function startScreenshot() {
  // var screenshotContainer = document.createElement("div");
  // screenshotContainer.setAttribute("id", "screenshot");
  var screenshotContainer = document.getElementById("screenshot");

  var overlay = document.createElement("div");
  overlay.classList.add("overlay");
  screenshotContainer.appendChild(overlay);

  var crosshairs = document.createElement("div");
  crosshairs.classList.add("crosshairs");
  screenshotContainer.appendChild(crosshairs);

  var borderedBox = document.createElement("div");
  borderedBox.classList.add("borderedBox");
  screenshotContainer.appendChild(borderedBox);

  var tooltip = document.createElement("span");
  tooltip.classList.add("tooltip");
  screenshotContainer.appendChild(tooltip);

  document.body.appendChild(screenshotContainer);

  new Screenshot();
}

class Screenshot {
  constructor() {
    this.overlay = document.getElementsByClassName("overlay")[0];
    this.crosshairs = document.getElementsByClassName("crosshairs")[0];
    this.borderedBox = document.getElementsByClassName("borderedBox")[0];
    this.tooltip = document.getElementsByClassName("tooltip")[0];
    this.mouseSelection = new MouseSelection(this.tooltip);

    this.mouseMoveHandler = this.mouseMoveHandler.bind(this);
    this.mouseDownHandler = this.mouseDownHandler.bind(this);
    this.mouseUpHandler = this.mouseUpHandler.bind(this);

    window.addEventListener("mousemove", this.mouseMoveHandler);
    window.addEventListener("mousedown", this.mouseDownHandler);
    window.addEventListener("mouseup", this.mouseUpHandler);
  }

  mouseMoveHandler(e) {
    this.mouseSelection.move(e);
    console.log("MOVE", this.mouseSelection);
    // this.updateOverlay();
    this.updateCrosshairs();
    this.updateBorderedBox();
    this.updateTooltip();
  }

  mouseDownHandler(e) {
    this.mouseSelection.mouseDown(e);
    console.log("DOWN", this.mouseSelection);
  }

  mouseUpHandler(e) {
    this.mouseSelection.mouseUp(e);
    console.log("UP", this.mouseSelection);
    window.removeEventListener("mousemove", this.mouseMoveHandler);
    window.removeEventListener("mousedown", this.mouseDownHandler);
    window.removeEventListener("mouseup", this.mouseUpHandler);
    this.removeScreenshotElements();
    this.takeScreenshot();
  }

  removeScreenshotElements() {
    var screenshotContainer = document.getElementById("screenshot");
    screenshotContainer.removeChild(this.overlay);
    screenshotContainer.removeChild(this.crosshairs);
    screenshotContainer.removeChild(this.borderedBox);
    screenshotContainer.removeChild(this.tooltip);
  }

  updateOverlay() {
    var mouseIsDown = this.mouseSelection.mouseIsDown;
    if (mouseIsDown && !this.overlay.classList.contains("highlighting")) {
      this.overlay.classList.add("highlighting");
    } else if (
      !mouseIsDown &&
      this.overlay.classList.contains("highlighting")
    ) {
      this.overlay.classList.remove("highlighting");
    }

    var overlayStyle = {
      borderWidth: this.mouseSelection.borderWidth
    };
    this.overlay.setAttribute("style", objectToStyle(overlayStyle));
  }

  updateCrosshairs() {
    var isDragging = this.mouseSelection.isDragging;
    if (isDragging && !this.crosshairs.classList.contains("hidden")) {
      this.crosshairs.classList.add("hidden");
    } else if (!isDragging && this.crosshairs.classList.contains("hidden")) {
      this.crosshairs.classList.remove("hidden");
    }

    var crosshairsStyle = {
      left: this.mouseSelection.crossHairsLeft + "px",
      top: this.mouseSelection.crossHairsTop + "px"
    };
    this.crosshairs.setAttribute("style", objectToStyle(crosshairsStyle));
  }

  updateBorderedBox() {
    var isDragging = this.mouseSelection.isDragging;
    if (!isDragging && !this.borderedBox.classList.contains("hidden")) {
      this.borderedBox.classList.add("hidden");
    } else if (isDragging && this.borderedBox.classList.contains("hidden")) {
      this.borderedBox.classList.remove("hidden");
    }

    var borderedBoxStyle = {
      left: this.mouseSelection.boxLeft + "px",
      top: this.mouseSelection.boxTop + "px",
      width: this.mouseSelection.boxEndWidth + "px",
      height: this.mouseSelection.boxEndHeight + "px"
    };
    this.borderedBox.setAttribute("style", objectToStyle(borderedBoxStyle));
  }

  updateTooltip() {
    var isDragging = this.mouseSelection.isDragging;
    if (!isDragging && !this.tooltip.classList.contains("hidden")) {
      this.tooltip.classList.add("hidden");
    } else if (isDragging && this.tooltip.classList.contains("hidden")) {
      this.tooltip.classList.remove("hidden");
    }

    var tooltipStyle = {
      left: this.mouseSelection.tooltipLeft + "px",
      top: this.mouseSelection.tooltipTop + "px"
    };
    this.tooltip.setAttribute("style", objectToStyle(tooltipStyle));
    this.tooltip.textContent =
      this.mouseSelection.boxEndWidth +
      "px " +
      this.mouseSelection.boxEndHeight +
      "px";
  }

  takeScreenshot() {
    html2canvas(document.querySelector("body")).then(canvas => {
      var croppedCanvas = document.createElement("canvas");
      var croppedCanvasContext = croppedCanvas.getContext("2d");

      croppedCanvas.width = this.mouseSelection.boxEndWidth;
      croppedCanvas.height = this.mouseSelection.boxEndHeight;

      croppedCanvasContext.drawImage(
        canvas,
        this.mouseSelection.startX,
        this.mouseSelection.startY,
        this.mouseSelection.boxEndWidth,
        this.mouseSelection.boxEndHeight,
        0,
        0,
        this.mouseSelection.boxEndWidth,
        this.mouseSelection.boxEndHeight
      );

      this.imageUrl = croppedCanvas
        .toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
      console.log(this.imageUrl);
      window.location.href = this.imageUrl;
    });
  }
}

class MouseSelection {
  constructor(tooltip) {
    this.mouseIsDown = false;
    this.isDragging = false;
    this.tookScreenshot = false; // after mouse is released

    this.startX = 0;
    this.startY = 0;
    this.endX = 0;
    this.endY = 0;

    this.borderWidth = "";

    this.crosshairsTop = 0;
    this.crosshairsLeft = 0;

    this.boxTop = 0;
    this.boxLeft = 0;
    this.boxEndWidth = 0;
    this.boxEndHeight = 0;

    this.tooltipMargin = +window
      .getComputedStyle(tooltip)
      .margin.split("px")[0];
    this.tooltip = tooltip;
    this.tooltipLeft = 0;
    this.tooltipTop = 0;
    this.tooltipWidth = this.tooltip.getBoundingClientRect().width;
    this.tooltipHeight = 0;

    this.calculateWindowSize();
    window.onResize = this.calculateWindowSize();
  }

  move(e) {
    this.crossHairsTop = e.clientY;
    this.crossHairsLeft = e.clientX;

    var tooltipBoundingRect = this.tooltip.getBoundingClientRect();
    this.tooltipWidth = tooltipBoundingRect.width;
    this.tooltipHeight = tooltipBoundingRect.height;

    if (this.mouseIsDown) {
      var endY = (this.endY = e.clientY);
      var endX = (this.endX = e.clientX);
      var startX = this.startX;
      var startY = this.startY;
      var windowWidth = this.windowWidth;
      var windowHeight = this.windowHeight;

      if (endX >= startX && endY >= startY) {
        this.isDragging = true;

        this.borderWidth =
          startY +
          "px " +
          (windowWidth - endX) +
          "px " +
          (windowHeight - endY) +
          "px " +
          startX +
          "px";

        this.boxTop = startY;
        this.boxLeft = startX;
        this.boxEndWidth = endX - startX;
        this.boxEndHeight = endY - startY;

        this.toolTipLeft = endX;
        this.toolTipTop = endY;

        if (endX + this.toolTipWidth >= windowWidth) {
          this.toolTipLeft =
            windowWidth - this.toolTipWidth - this.tooltipMargin * 2;
        }

        if (
          endY + this.toolTipHeight + this.tooltipMargin * 2 >=
          windowHeight
        ) {
          this.toolTipTop =
            windowHeight - this.toolTipHeight - this.tooltipMargin * 2;
        }
      } else if (endX <= startX && endY >= startY) {
        this.isDragging = true;

        this.borderWidth =
          startY +
          "px " +
          (windowWidth - startX) +
          "px " +
          (windowHeight - endY) +
          "px " +
          endX +
          "px";

        this.boxLeft = endX;
        this.boxTop = startY;
        this.boxEndWidth = startX - endX;
        this.boxEndHeight = endY - startY;

        this.toolTipLeft = endX - this.toolTipWidth;
        this.toolTipTop = endY;

        if (endX - this.toolTipWidth <= 0) {
          this.toolTipLeft = this.tooltipMargin;
        }

        if (
          endY + this.toolTipHeight + this.tooltipMargin * 2 >=
          windowHeight
        ) {
          this.toolTipTop =
            windowHeight - this.toolTipHeight - this.tooltipMargin * 2;
        }
      } else if (endX >= startX && endY <= startY) {
        this.isDragging = true;

        this.boxLeft = startX;
        this.boxTop = endY;
        this.boxEndWidth = endX - startX;
        this.boxEndHeight = startY - endY;

        this.toolTipLeft = endX;
        this.toolTipTop = endY - this.toolTipHeight;

        this.borderWidth =
          endY +
          "px " +
          (windowWidth - endX) +
          "px " +
          (windowHeight - startY) +
          "px " +
          startX +
          "px";

        if (endX + this.toolTipWidth >= windowWidth) {
          this.toolTipLeft =
            windowWidth - this.toolTipWidth - this.tooltipMargin * 2;
        }

        if (endY - this.toolTipHeight <= 0) {
          this.toolTipTop = this.tooltipMargin;
        }
      } else if (endX <= startX && endY <= startY) {
        this.isDragging = true;

        this.boxLeft = endX;
        this.boxTop = endY;
        this.boxEndWidth = startX - endX;
        this.boxEndHeight = startY - endY;

        this.borderWidth =
          endY +
          "px " +
          (windowWidth - startX) +
          "px " +
          (windowHeight - startY) +
          "px " +
          endX +
          "px";

        this.toolTipLeft = endX - this.toolTipWidth;
        this.toolTipTop = endY - this.toolTipHeight;

        if (endX - this.toolTipWidth <= 0) {
          this.toolTipLeft = this.tooltipMargin;
        }

        if (endY - this.toolTipHeight <= 0) {
          this.toolTipTop = this.tooltipMargin;
        }
      } else {
        this.isDragging = false;
      }
    }
  }

  mouseDown(e) {
    this.borderWidth = this.windowWidth + "px " + this.windowHeight + "px";

    this.startX = e.clientX;
    this.startY = e.clientY;

    this.toolTipWidth = this.tooltip.getBoundingClientRect().width;

    this.mouseIsDown = true;
    this.tookScreenShot = false;
  }

  mouseUp(e) {
    this.borderWidth = 0;

    if (this.isDragging) {
      // Don't take the screenshot unless the mouse moved somehow.
      this.tookScreenShot = true;
    }

    this.isDragging = false;
    this.mouseIsDown = false;
  }

  calculateWindowSize() {
    this.windowHeight =
      window.innerHeight ||
      document.documentElement.clientHeight ||
      document.body.clientHeight;
    this.windowWidth =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;
  }
}
